resource "aws_lambda_layer_version" "dependency_layer" {
  filename            = "${path.module}/layer.zip"
  layer_name          = "dependencies_layer"
  source_code_hash    = filebase64sha256("${path.module}/layer.zip")
  compatible_runtimes = ["python3.11"]
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  output_path = "${path.module}/deployment_package.zip"

  source {
    content  = file("${path.module}/../src/main.py")
    filename = "main.py"
  }
}

resource "aws_lambda_function" "slack_launch_bot" {
  filename         = data.archive_file.lambda_zip.output_path
  function_name    = "SlackLaunchBot"
  handler          = "main.lambda_handler"
  layers           = [aws_lambda_layer_version.dependency_layer.arn]
  memory_size      = 512
  role             = aws_iam_role.lambda_execution_role.arn
  runtime          = "python3.11"
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  timeout          = "60"
  environment {
    variables = {
      ENVIRONMENT = "prod"
    }
  }
  tracing_config {
    mode = "Active"
  }
}
