resource "aws_cloudwatch_event_rule" "cron_rule" {
  name                = "slack-launch-bot-lambda-cron-rule"
  description         = "Triggers the Slack Launch Bot Lambda function"
  schedule_expression = "cron(0 13 * * ? *)"
}

resource "aws_cloudwatch_event_target" "invoke_lambda" {
  arn       = aws_lambda_function.slack_launch_bot.arn
  rule      = aws_cloudwatch_event_rule.cron_rule.name
  target_id = "invokeLambdaFunction"
}
