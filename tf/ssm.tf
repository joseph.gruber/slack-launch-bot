# ================== Test Parameters ================== #
resource "aws_ssm_parameter" "test_slack_bot_token" {
  description = "Slack bot token"
  name        = "/test/slack-bot-token"
  type        = "SecureString"
  value       = "placeholder"
  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_ssm_parameter" "test_slack_channel_id" {
  description = "Slack channel id for posting messages"
  name        = "/test/slack-channel-id"
  type        = "String"
  value       = "placeholder"
  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_ssm_parameter" "test_launch_library_base_url" {
  description = "Base URL for the Launch Library API"
  name        = "/test/launch-library-base-url"
  type        = "String"
  value       = "https://lldev.thespacedevs.com/"
  lifecycle {
    ignore_changes = [value]
  }
}

# ================== Prod Parameters ================== #
resource "aws_ssm_parameter" "prod_slack_bot_token" {
  description = "Slack bot token"
  name        = "/prod/slack-bot-token"
  type        = "SecureString"
  value       = "placeholder"
  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_ssm_parameter" "prod_slack_channel_id" {
  description = "Slack channel id for posting messages"
  name        = "/prod/slack-channel-id"
  type        = "String"
  value       = "placeholder"
  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_ssm_parameter" "prod_launch_library_base_url" {
  description = "Base URL for Launch Library API"
  name        = "/prod/launch-library-base-url"
  type        = "String"
  value       = "https://ll.thespacedevs.com/2.2.0/"
  lifecycle {
    ignore_changes = [value]
  }
}
