# Slack Launch Bot for Space Network

Welcome to the Slack Launch Bot for Space Network! This project is designed to provide automated Slack notifications for launch events using the [Launch Library API](https://thespacedevs.com/llapi). Automated notifications are posted in the [#launches](https://space-comm.slack.com/archives/CUVMXKYSK) daily at 1300Z.

## Contributing

Thank you for your interest in contributing to the Slack Launch Bot! We welcome contributions from everyone, and we are grateful for every contribution made to this project. This section will guide you through the contribution process.

### Getting Started

1. **Clone the Repository**

    - Clone the repository to your local machine to start making changes. You can do this with the following command:
        ```
        git clone git@gitlab.com:joseph.gruber/slack-launch-bot.git
        ```
    - Navigate into the cloned directory.

2. **Create a New Branch**
    - Before making any changes, create a new branch on your local reposistory to keep your contributions organized.
        ```
        git checkout -b your-branch-name
        ```

### Making Changes

1. **Make Your Changes**

    - With your branch created, you're ready to make the changes you want. Feel free to add new features, fix bugs, or improve documentation. Please make sure to follow the coding standards and guidelines of the project.

2. **Commit Your Changes**

    - After making your changes, commit them to your branch. Make sure your commit messages are clear and descriptive.
        ```
        git commit -am 'Add some feature'
        ```

3. **Keep Your Branch Updated**
    - It's important to keep your branch up to date with the main project's master branch. Regularly fetch and merge changes from the master branch into your branch.
        ```
        git pull origin main
        ```

### Submitting a Merge Request

1. **Push Changes to Gitlab**

    - Once you're happy with your changes, push them to Gitlab.
        ```
        git push origin your-branch-name
        ```

2. **Create a Merge Request**

    - Go to the repository on [Gitlab](https://gitlab.com/joseph.gruber/slack-launch-bot). You'll see a "Compare & pull request" button for your branch. Click it to start creating a merge request.
    - Provide a clear and detailed description of your changes. Explain why you made the changes and how they impact the project.

3. **Review Process**
    - Once your merge request is submitted, it will be reviewed by the project maintainers. Be open to feedback and be prepared to make further changes if requested. This is a collaborative process intended to ensure the quality and coherence of the project.

### After Your Merge Request is Merged

After your merge request has been accepted and merged, your changes will be part of the project. We encourage you to stay involved. There are always new issues to work on, and your continued contributions are valuable to the project.

### Need Help?

If you need help or have any questions, feel free to reach out. You can open an issue on Gitlab or contact the project maintainers directly if you have specific questions about contributions.
