import json
import logging
import os
import urllib.parse
from datetime import datetime, timedelta, timezone
from time import mktime
from typing import Any

import boto3
from requests import get
from slack_bolt import App
from slack_sdk.errors import SlackApiError

# Setup logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    # Get the various parameters from AWS Parameter Store
    parameters = get_parameters()

    # Initialize the constant variables
    LAUNCH_LIBRARY_API_BASE_URL = parameters.get("launch-library-base-url")
    SLACK_BOT_TOKEN = parameters.get("slack-bot-token")
    SLACK_CHANNEL_ID = parameters.get("slack-channel-id")

    # If we're missing values for the parameters, we'll fail the Lambda
    if LAUNCH_LIBRARY_API_BASE_URL is None or SLACK_BOT_TOKEN is None or SLACK_CHANNEL_ID is None:
        raise ValueError("One or more required configurations are missing.")

    # Initialize the Slack app
    app = App(token=SLACK_BOT_TOKEN)

    try:
        launches = get_launches(base_url=LAUNCH_LIBRARY_API_BASE_URL)
        for launch in launches:
            post_to_slack(launch=launch, slack_app=app, slack_channel_id=SLACK_CHANNEL_ID)

        # Signal to Lambda the succesful completion of the function
        return {"statusCode": 200, "body": "Lambda executed successsfully"}
    except Exception as err:
        # If a known exception, we use the structure log format to log the exception
        try:
            error_info = json.loads(str(err))
            logger.error(error_info["message"], extra={"error": error_info})
        # or if it's an unknown exception, we convert the error to string and log a generic exception message
        except json.JSONDecodeError:
            logger.exception("An unknown exception occurred: %s", str(err))


def get_parameters() -> dict[str, Any]:
    """Gets the defined parameters from AWS Parameter Store for the configuration of the Lambda function

    Returns:
        Dictionary of parameters and their values
    """
    # Determine the environment and construct the parameter path
    environment_path = os.environ.get("ENVIRONMENT", "prod")  # Default to 'prod' if not set
    parameter_path = f"/{environment_path}/"

    # Retrieve parameters from AWS Parameter Store
    current_region = os.environ["AWS_REGION"]
    ssm = boto3.client("ssm", region_name=current_region)

    # Retrieve all parameters for the environment and convert to a dictionary of parameter name/values
    parameters = ssm.get_parameters_by_path(Path=parameter_path, WithDecryption=True)
    parameters_dict = {param["Name"].split("/")[-1]: param["Value"] for param in parameters["Parameters"]}

    return parameters_dict


def get_launches(base_url: str) -> list[dict[str, Any]]:
    """Gets the list of launches for the next one day from the Launch Library API

    Keyword Arguments:
        base_url -- Base URL for the Launch Library API endpoint

    Raises:
        Exception: For any non-200 responses from the API

    Returns:
        List of launches
    """
    net = datetime.now(tz=timezone.utc) + timedelta(days=1)
    url_params = (
        "launch/upcoming/?format=json&"
        "hide_recent_previous=true&"
        "include_suborbital=true&"
        f"net__lte={net.strftime('%Y-%m-%dT%H:%M:%SZ')}&"
        "ordering=window_start"
    )

    request_url = urllib.parse.urljoin(base=base_url, url=url_params)
    response = get(request_url, timeout=30)

    if response.status_code == 200:
        data = response.json()

        launch_count = data.get("count", "unknown")
        now_iso = datetime.now(tz=timezone.utc).isoformat()
        logger.info(f"Received {str(launch_count)} launches for {now_iso} to {net.isoformat()}")

        return data.get("results", [])

    # We raise an exception on any non-200 status codes so that we log it to Cloudwatch for further review
    raise Exception(
        json.dumps(
            {
                "message": "Failed to retrieve latest launches from Launch Library API.",
                "headers": response.headers,
                "reason": response.reason,
                "status_code": response.status_code,
                "url": response.url,
            }
        )
    )


def post_to_slack(launch: dict, slack_app: App, slack_channel_id: str) -> None:
    """Posts a notification to a Slack channel of a launch event

    Arguments:
        launch -- Dictionary of launch event
        slack_app -- Slack Bolt app
        slack_channel_id -- Channel to post the message to
    """
    block_kit = create_block_kit(launch=launch)

    try:
        # Send the block kit message to the Slack channel
        result = slack_app.client.chat_postMessage(
            channel=slack_channel_id,
            blocks=block_kit,
            text=f"🚀🚀🚀 Launch Notification: {launch.get('name', '')} 🚀🚀🚀",
        )

        # If the posting to Slack was ok, we log the result as info. Otherwise, we log an error
        # We don't raise any exceptions or exit from the flow so as to allow any other Slack
        # posts to continue and attempt to post
        if result.get("ok"):
            logger.info(result)
        else:
            logger.error(f"Failed to post message to Slack: {result}")
    except SlackApiError as err:
        logger.error(f"Error posting message to Slack: {err}")


def create_block_kit(launch: dict) -> list[dict[str, Any]]:
    """Generates a Slack Block Kit formatted message of a launch notification

    Arguments:
        launch -- Dictionary of launch event

    Returns:
        Block kit formatted string of the launch notification
    """
    name = launch.get("name", "Unknown")
    image_url = launch.get("image", "https://upload.wikimedia.org/wikipedia/en/4/48/Blank.JPG")
    window_start = launch.get("window_start", "Unknown")
    window_start_dt = datetime.fromisoformat(window_start).replace(tzinfo=None)
    window_start_ts = int(mktime(window_start_dt.timetuple()))
    window_end = launch.get("window_end", "Unknown")
    window_end_dt = datetime.fromisoformat(window_end).replace(tzinfo=None)
    window_end_ts = int(mktime(window_end_dt.timetuple()))
    mission_description = launch.get("mission", {}).get("description", "Unknown")
    pad_name = launch.get("pad", {}).get("name", "Unknown")
    vehicle_name = launch.get("rocket", {}).get("configuration", {}).get("name", "Unknown")
    launch_provider = launch.get("launch_service_provider", {}).get("name", "Unknown")

    # Return a formatted Slack Block Kit message
    return [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": f":rocket: *{name}*\n{mission_description}",
            },
            "accessory": {
                "type": "image",
                "image_url": image_url,
                "alt_text": name,
            },
        },
        {
            "type": "section",
            "fields": [
                {
                    "type": "mrkdwn",
                    "text": f"*Window Open:*\n<!date^{window_start_ts}^{{date_short_pretty}} {{time_secs}}|{window_start}>",
                },
                {
                    "type": "mrkdwn",
                    "text": f"*Window Close:*\n<!date^{window_end_ts}^{{date_short_pretty}} {{time_secs}}|{window_end}>",
                },
                {"type": "mrkdwn", "text": f"*Vehicle:*\n{vehicle_name}"},
                {"type": "mrkdwn", "text": f"*Provider:*\n{launch_provider}"},
                {"type": "mrkdwn", "text": f"*Launch Pad:*\n{pad_name}"},
            ],
        },
    ]
