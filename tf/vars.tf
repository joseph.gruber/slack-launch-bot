variable "aws_region" {
  type        = string
  description = "Default region for AWS resources"
  default     = "us-east-1"
}

variable "default_tags" {
  type        = map(string)
  description = "Default tags to be added to all AWS resources"
  default = {
    Project = "slack-launch-bot"
  }
}
